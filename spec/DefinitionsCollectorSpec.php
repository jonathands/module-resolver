<?php

namespace spec\Paneric\ModuleResolver;

use Paneric\ModuleResolver\DefinitionsCollector;
use PhpSpec\ObjectBehavior;

class DefinitionsCollectorSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(DefinitionsCollector::class);
    }

    public function it_sets_website_settings(): void
    {
        $rooFolder = __DIR__ . '/';
        $appFolder = $rooFolder . 'src/';

        $settings = $this->setDefinitions(
            $appFolder,
            $appFolder . 'Website/',
            'definitions',
            'en',
            'prod'
        );
        $settings->shouldHaveKeyWithValue('orm_key', 'orm_value');
        $settings->shouldHaveKeyWithValue('twig_key', 'twig_value');
        $settings->shouldHaveKeyWithValue('prod', ['prod_key' => 'prod_value']);
        $settings->shouldHaveKeyWithValue('translation_app', ['translation_app_key' => 'translation_app_value']);
        $settings->shouldHaveKeyWithValue('translation_mod', ['translation_mod_key' => 'translation_mod_value']);
        $settings->shouldHaveKeyWithValue('controller_key', 'controller_value');
        $settings->shouldHaveKeyWithValue('repository_key', 'repository_value');
        $settings->shouldHaveKeyWithValue('service_key', 'service_value');

        $settings->shouldNotHaveKeyWithValue('validation', ['validation_key' => 'validation_value']);
        $settings->shouldNotHaveKeyWithValue('dev_key', ['dev_key' => 'dev_value']);
        $settings->shouldNotHaveKeyWithValue('middleware_key', 'middleware_value');
        $settings->shouldNotHaveKeyWithValue('package_key', 'package_value');
    }

    public function it_sets_registration_settings(): void
    {
        $rooFolder = __DIR__ . '/';
        $appFolder = $rooFolder . 'src/';

        $settings = $this->setDefinitions(
            $appFolder,
            $appFolder . 'Registration/',
            'definitions',
            'en',
            'dev'
        );
        $settings->shouldHaveKeyWithValue('orm_key', 'orm_value');
        $settings->shouldHaveKeyWithValue('twig_key', 'twig_value');
        $settings->shouldHaveKeyWithValue('dev', ['dev_key' => 'dev_value']);
        $settings->shouldHaveKeyWithValue('translation_app', ['translation_app_key' => 'translation_app_value']);
        $settings->shouldHaveKeyWithValue('controller_key', 'controller_value');
        $settings->shouldHaveKeyWithValue('middleware_key', 'middleware_value');
        $settings->shouldHaveKeyWithValue('package_key', 'package_value');
        $settings->shouldHaveKeyWithValue('validation', ['validation_key' => 'validation_value']);

        $settings->shouldNotHaveKeyWithValue('prod_key', ['prod_key' => 'prod_value']);
        $settings->shouldNotHaveKeyWithValue('repository_key', 'repository_value');
        $settings->shouldNotHaveKeyWithValue('service_key', 'service_value');
    }
}
