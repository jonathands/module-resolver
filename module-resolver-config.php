<?php

return [
    'default_route_key' => 'home',
    'local_map' => ['fr', 'nl', 'de', 'en', 'pl'],
    'module_map' => [
        'error' => 'Error',
        'home' => 'Website',
        'signin' => 'Authentication',
        'role' => 'Authorization',
    ],
];
