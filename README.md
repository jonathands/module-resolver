# Module Resolver

Simple, yet still useful, tool for specifying module's folder name in case of 
modular project architecture, accompanied by dynamic (no config required) definitions collector.

## Required

* PHP 7.1+

## File structure

* ModuleResolver.php
* DefinitionsCollector.php
* module-resolver-config.php *(an example of required configuration)*

## Installation

### composer
```sh
composer require paneric/module-resolver:2.0.0
```

## Integration

Slim Framework/PHP-DI example (ver.4)

### configuration
In case of project's modular structure, it is highly possible to relay on "double scoped" definitions (the first, 
common for an application itself and the second, that is specific for a module). Please copy (and rename if you wish)
module-resolver-config.php file, paste it into your application config folder (module(s) will consist separate one) and
adapt it by the follwing remarks: 
```php
return [
    'default_route_key' => 'home',
    'language_map' => ['fr', 'nl', 'de', 'en', 'pl'],
    'module_map' => [
        'error' => 'Error',
        'home' => 'Website',
        'signin' => 'Authentication',
        'role' => 'Authorization',
        ...
    ],
];
```

* 'default_route_key' - not to much to explain;
* 'language_map' - an array of language version markers intended to be used within your routes;
* 'module_map' - an array/mapper of Modules names related to the first segment of a route.

**ProTip:** as a consequence, above example of configuration: **'home' => 'Website',** will establish **Website/** as 
a module folder name for following examples of routes: **/home,  /home/fr** or **/home/sth/fr** .

### front controller

Let's consider an example of some modular project structure as follows:

* my_project
    * **public**
        * index.php
    * **src**
        * **config**
            * module-resolver-config.php
        * **definitions**
            * **di**      
                * orm.php            
                * twig.php
                * ...            
            * **settings**
                * dev.php
                * prod.php
                * translation.en.php
                * ...      
            * ...
        * **Website**
            * **definitions**
                * **di**      
                    * controller.php            
                    * repository.php
                    * service.php                
                    * ...              
                * **settings**
                    * translation.en.php
                    * ...           
            * **Entity**
            * **Controller**
            * **Repository**
            * **templates**
            * **...**            
        * **Registration**
            * **definitions**
                * **di**      
                    * controller.php            
                    * middleware.php
                    * package.php                
                    * ...              
                * **settings**
                    * validation.php
                    * ...  
            * ...
        * ...
            
In this case our practical integration of the module resolver may look this way:
 
```php
use Paneric\ModuleResolver\ModuleResolver;
use Paneric\ModuleResolver\DefinitionsCollector;
use DI\ContainerBuilder;
use Slim\Factory\AppFactory;

define('ENV', 'dev');

define('ROOT_FOLDER', dirname(__DIR__) . '/');
define('APP_FOLDER', ROOT_FOLDER . 'src/');

require ROOT_FOLDER . 'vendor/autoload.php';

try{
    $moduleResolverConfig = require_once APP_FOLDER . 'config/module_resolver_config.php';
    $moduleResolver = new ModuleResolver();
    $moduleFolderName = $moduleResolver->setModuleFolderName(
        $_SERVER['REQUEST_URI'], 
        $moduleResolverConfig
    );

    $definitionsCollector = new DefinitionsCollector();
    $definitions = $definitionsCollector->setDefinitions(
        APP_FOLDER,
        APP_FOLDER . $moduleFolderName, 
        'definitions'
    );

    $builder = new ContainerBuilder();
    $builder->addDefinitions($definitions);
    $container = $builder->build();

    AppFactory::setContainer($container);
    $app = AppFactory::create();

    // ... (any other require) //

    $app->run();
} catch (Exception $e) {
   echo $e->getMessage();
}
```
**ProTip:** ** **definitions** states for folder name of php-di definitions. Only its sub-folders (1 st level 
exclusively) are scanned for .php definitions files (returning array).

If you need some environment variables to include in your settings file, it has to be named according to your 
environment variable value (dev or prod) set in the front controller. The env value is passed to the definitions 
collector method as the last parameter (optional, default value is set to null). 

It works the the same in case of translation settings related to "local" method parameter (also optional, default 
parameter value is set to null) under one condition however. Your translation settings file name has to follow a 
particular convention.
Regardles of the location (application settings folder or module settings folder) it has to be for example 
"**translation.en.php**" where "en" is actual local value for "English". If you set translation files both in 
src/definitions/settings and src/Module/definitions/settings remember to merge appropriate arrays during translator 
instantiation. 

To make a long story short it will be necessary to modify settings collector "setSettings" method call as:

```php
    $settingsCollector = new SettingsCollector();
    $settings = $settingsCollector->setSettings(
        APP_FOLDER,
        APP_FOLDER . $moduleFolderName, 
        'definitions',
        $local
    );
```
if you need only some translation dictionary, or:
```php
    $settingsCollector = new SettingsCollector();
    $settings = $settingsCollector->setSettings(
        APP_FOLDER,
        APP_FOLDER . $moduleFolderName, 
        'definitions',
        $local, 
        ENV
    );
```
if you need some translation dictionary along with some environmental settings, or:
```php
    $settingsCollector = new SettingsCollector();
    $settings = $settingsCollector->setSettings(
        APP_FOLDER,
        APP_FOLDER . $moduleFolderName, 
        'definitions',
        '', 
        ENV
    );
```
if you need only some environmental settings.


**ProTip:** **"... (any other require)"** mentioned in the front controller code related to separated dependencies, 
routes etc can be realized by the following manner:

```php
     // twig, orm
    require_once APP_FOLDER . 'definitions/dependencies.php';

    // controllers, repositories, middlewares, extensions, etc;
    require_once APP_FOLDER . $moduleFolderName . 'definitions/dependencies.php';

    // and of course routes
    require_once APP_FOLDER . $moduleFolderName . 'definitions/routes.php';
```

