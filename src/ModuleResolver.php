<?php

declare(strict_types=1);

namespace Paneric\ModuleResolver;

class ModuleResolver
{
    private $local;

    public function setModuleFolderName(string $requestUri, array $config): string
    {
        $defaultRouteKey = $config['default_route_key'];
        $localMap = $config['local_map'];
        $moduleMap = $config['module_map'];

        $uriSegments = explode('/', ltrim($requestUri, '/'));

        if (in_array(strtolower(end($uriSegments)), $localMap, true)){
            $this->local = array_pop($uriSegments);
        }

        if (empty($uriSegments)) {
            return $moduleMap[$defaultRouteKey] . '/';
        }

        $moduleKey = strtolower($uriSegments[0]);

        if ($moduleKey === '') {
            return $moduleMap[$defaultRouteKey] . '/';
        }

        if (array_key_exists($moduleKey, $moduleMap)) {
            return $moduleMap[$moduleKey] . '/';
        }

        return $moduleMap[$defaultRouteKey] . '/';
    }

    public function getLocal(): ?string
    {
        return $this->local;
    }
}
